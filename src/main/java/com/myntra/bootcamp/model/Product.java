package com.myntra.bootcamp.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {

    private String id;
    private String name;
    private int cost;
    private String category;
    private String brand;
}