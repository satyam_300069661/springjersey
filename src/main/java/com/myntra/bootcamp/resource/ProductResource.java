package com.myntra.bootcamp.resource;

import com.myntra.bootcamp.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.*;

@Component
@Path("/v1/product")
public class ProductResource {

    Map<String, Product> productMap;
    List<Product> productList;

    @Autowired
    public ProductResource() {

        productMap = new HashMap<>();
        productList = new LinkedList<>();
    }

    @POST
    @Path("/create")
    @Produces("application/json")
    @Consumes("application/json")
    public Response createProduct(Product p) {
        final String uuid = UUID.randomUUID().toString();
        p.setId(uuid);
        productMap.put(uuid, p);
        productList.add(p);
        return Response.ok(p).build();
    }

    @GET
    @Path("/all")
    @Produces("application/json")
    public Response getProductDetails() {
        return Response.ok(productList).build();
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getProductDetailForId(@PathParam("id") String id) {
        return Response.ok(productMap.getOrDefault(id, null)).build();
    }

}
